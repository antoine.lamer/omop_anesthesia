# Anesthesia dashboards with OMOP CDM

This repository contains developments about anesthesia dashboards and with OMOP CDM. The objectives of these dashboards are to display indicators and graphics for unit management, quality of care or research.

The folder **ddl** constains SQL scripts that query OMOP CDM and feed new tables for the dashboards.

The folder **shinydashboard** contains R scripts generating shiny dashboards.



![](https://framagit.org/antoine.lamer/omop_anesthesia/raw/master/images/dashboard_population.png)

![](https://framagit.org/antoine.lamer/omop_anesthesia/raw/master/images/dashboard_ventilation.png)
