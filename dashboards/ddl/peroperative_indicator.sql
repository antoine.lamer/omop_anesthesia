
drop table if exists reporting.peroperative_indicator;

create table if not exists reporting.peroperative_indicator (
	person_id 				integer not null
	, visit_occurrence_id 	integer not null
	, visit_detail_id 		integer not null
	, visit_detail_start_date date
	, mean_map_anesthesia	numeric
	, mean_hr_anesthesia	numeric
	, mean_od_anesthesia	numeric
	, mean_itd_anesthesia	numeric
	, mean_itd_surgery		numeric
	, mean_etd_anesthesia	numeric
	, mean_etd_surgery		numeric
);




delete from reporting.peroperative_indicator;

with operation as(
	select person_id
	, visit_occurrence_id
	, visit_detail_id
	, visit_detail_start_date
	from omop.visit_detail vd
	where vd.visit_detail_concept_id = 2100000000
), pam_indicator as (
	select o.visit_detail_id, m.value_as_number as mean_map_anesthesia
	from operation o 
	left outer join omop.measurement m
	on o.visit_detail_id = m.visit_detail_id
	inner join omop.concept c
	on m.measurement_source_concept_id = c.concept_id
	and c.concept_code = '31'
	and c.vocabulary_id = 'Lille Indicator'
), hr_indicator as (
	select o.visit_detail_id, m.value_as_number as mean_hr_anesthesia
	from operation o 
	left outer join omop.measurement m
	on o.visit_detail_id = m.visit_detail_id
	inner join omop.concept c
	on m.measurement_source_concept_id = c.concept_id
	and c.concept_code = '23'
	and c.vocabulary_id = 'Lille Indicator'
), od_indicator as (
	select o.visit_detail_id, m.value_as_number as mean_od_anesthesia
	from operation o 
	left outer join omop.measurement m
	on o.visit_detail_id = m.visit_detail_id
	inner join omop.concept c
	on m.measurement_source_concept_id = c.concept_id
	and c.concept_code = '27'
	and c.vocabulary_id = 'Lille Indicator'
), mean_etd_surgery as (
	select o.visit_detail_id, m.value_as_number as mean_etd_surgery
	from operation o 
	left outer join omop.measurement m
	on o.visit_detail_id = m.visit_detail_id
	inner join omop.concept c
	on m.measurement_source_concept_id = c.concept_id
	and c.concept_code = '415'
	and c.vocabulary_id = 'Lille Indicator'
)
insert into reporting.peroperative_indicator
select o.person_id::integer
		, o.visit_occurrence_id::integer
		, o.visit_detail_id::integer
		, o.visit_detail_start_date::date
		, pam_indicator.mean_map_anesthesia
		, hr_indicator.mean_hr_anesthesia
		, od_indicator.mean_od_anesthesia
		, null::numeric
		, null::numeric
		, null::numeric
		, mean_etd_surgery.mean_etd_surgery
from operation o
left outer join pam_indicator
on o.visit_detail_id = pam_indicator.visit_detail_id
left outer join hr_indicator
on o.visit_detail_id = hr_indicator.visit_detail_id
left outer join od_indicator
on o.visit_detail_id = od_indicator.visit_detail_id
left outer join mean_etd_surgery
on o.visit_detail_id = mean_etd_surgery.visit_detail_id
;

select * from reporting.peroperative_indicator 
where mean_etd_surgery is not null
limit 100;

select count(*) from reporting.peroperative_indicator
limit 100;



