------------------------------------------------------------------------------------------------------------------
-- Vocabulary loading for anesthesia procedure
--
-- Antoine Lamer
--
-- 25/01/2020
--
-- 1_concept_vocabulary_lille.csv -> concepts related to Lille vocabularies
-- 2_vocabulary_lille.csv -> Lille Vocabularies
-- 3_concept_relationship_lille.csv -> concepts related to Lille relationships
-- 4_relationship_lille.csv -> Lille relationships
-- 5_1_aims_unit_lille.csv -> units from AIMS
-- 5_2_aims_measurement_lille.csv -> measurements from AIMS
-- 5_3_aims_episode_lille.csv
------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------
-- 1) concept_vocabulary_lille
------------------------------------------------------------------------------------------------------------------

-- Creation of concept_vocabulary_lille
------------------------------------------------------------------------------------------------------------------

DROP TABLE if exists etl.concept_vocabulary_lille;

CREATE TABLE if not exists etl.concept_vocabulary_lille
 (
  concept_id			INTEGER  		NOT NULL,
  concept_name		    VARCHAR 	    NOT NULL,
  domain_id				VARCHAR(20)		NOT NULL,
  vocabulary_id			VARCHAR(20)		NOT NULL,
  concept_class_id		VARCHAR(20)		NOT NULL,
  standard_concept		VARCHAR(1)		NULL,
  concept_code			VARCHAR(50)		NOT NULL,
  valid_start_date		DATE			NOT NULL,
  valid_end_date		DATE			NOT NULL,
  invalid_reason		VARCHAR(1)		NULL
);

delete from etl.concept_vocabulary_lille;


-- Insertion of csv file into concept_vocabulary_lille
------------------------------------------------------------------------------------------------------------------

COPY etl.concept_vocabulary_lille
from '/home/ant/Documents/01_projets/OMOP_Anesthesie/anesthesie_omop/etl/01_local_vocabularies/lille_vocabulary/1_concept_vocabulary_lille.csv' 
DELIMITER ';' CSV HEADER;

-- Vérifications du chargement
select * from etl.concept_vocabulary_lille;

-- Insertion of concept_vocabulary_lille into concept
------------------------------------------------------------------------------------------------------------------

INSERT INTO omop.concept
SELECT * FROM etl.concept_vocabulary_lille;

select * FROM omop.concept where concept_id in (select concept_id from etl.concept_vocabulary_lille);

DELETE FROM omop.concept where concept_id in (select concept_id from etl.concept_vocabulary_lille);


------------------------------------------------------------------------------------------------------------------
-- 2) vocabulary_lille
------------------------------------------------------------------------------------------------------------------

-- Creation of vocabulary_lille
------------------------------------------------------------------------------------------------------------------

DROP TABLE if exists etl.vocabulary_lille;

CREATE TABLE if not exists etl.vocabulary_lille 
(
  vocabulary_id				VARCHAR(20)		NOT NULL,
  vocabulary_name			VARCHAR(255)	NOT NULL,
  vocabulary_reference		VARCHAR(255)	NULL,
  vocabulary_version		VARCHAR(255)	NULL,
  vocabulary_concept_id		INTEGER			NOT NULL
)
;

-- insertion of csv file into vocabulary_lille
------------------------------------------------------------------------------------------------------------------

select * from etl.vocabulary_lille;

delete from etl.vocabulary_lille;

COPY etl.vocabulary_lille
from '/home/ant/Documents/01_projets/OMOP_Anesthesie/anesthesie_omop/etl/01_local_vocabularies/lille_vocabulary/2_vocabulary_lille.csv' 
DELIMITER ';' CSV HEADER;

select * from etl.vocabulary_lille;

-- Insertion of vocabulary_lille into vocabulary
------------------------------------------------------------------------------------------------------------------

SELECT * FROM omop.vocabulary where vocabulary_id in (select vocabulary_id from etl.vocabulary_lille);

DELETE FROM omop.vocabulary where vocabulary_id in (select vocabulary_id from etl.vocabulary_lille);

INSERT INTO omop.vocabulary
SELECT * FROM etl.vocabulary_lille;

------------------------------------------------------------------------------------------------------------------
-- 3) concept_relationship_lille
------------------------------------------------------------------------------------------------------------------

-- Creation of concept_relationship_lille relationship for Lille AIMS
------------------------------------------------------------------------------------------------------------------

DROP TABLE if exists etl.concept_relationship_lille;

CREATE TABLE if not exists etl.concept_relationship_lille
 (
  concept_id			INTEGER  		,
  concept_name		    VARCHAR 	    NOT NULL,
  domain_id				VARCHAR(20)		NOT NULL,
  vocabulary_id			VARCHAR(20)		NOT NULL,
  concept_class_id		VARCHAR(20)		NOT NULL,
  standard_concept		VARCHAR(1)		NULL,
  concept_code			VARCHAR(50)		NOT NULL,
  valid_start_date		DATE			NOT NULL,
  valid_end_date		DATE			NOT NULL,
  invalid_reason		VARCHAR(1)		NULL
);

delete from etl.concept_relationship_lille;

-- Insertion of csv file into concept_relationship_lille
------------------------------------------------------------------------------------------------------------------

COPY etl.concept_relationship_lille
from '/home/ant/Documents/01_projets/OMOP_Anesthesie/anesthesie_omop/etl/01_local_vocabularies/lille_vocabulary/3_concept_relationship_lille.csv' 
DELIMITER ';' CSV HEADER;

select * from etl.concept_relationship_lille;

-- Insertion of concept_relationship_lille into concept
------------------------------------------------------------------------------------------------------------------

SELECT * FROM omop.concept where concept_id in (select concept_id from etl.concept_relationship_lille);

DELETE FROM omop.concept where concept_id in (select concept_id from etl.concept_relationship_lille);

INSERT INTO omop.concept
SELECT * FROM etl.concept_relationship_lille;

------------------------------------------------------------------------------------------------------------------
-- 4) relationship_lille
------------------------------------------------------------------------------------------------------------------

DROP TABLE if exists etl.relationship_lille;

CREATE TABLE if not exists etl.relationship_lille
(
  relationship_id 				character varying(20) NOT NULL,
  relationship_name 			character varying(255) NOT NULL,
  is_hierarchical 				character varying(1) NOT NULL,
  defines_ancestry 				character varying(1) NOT NULL,
  reverse_relationship_id 		character varying(20) NOT NULL,
  relationship_concept_id 		integer NOT NULL,
  CONSTRAINT xpk_relationship PRIMARY KEY (relationship_id)
);

delete from etl.relationship_lille;

-- Insertion of csv file into relationship_lille
------------------------------------------------------------------------------------------------------------------

COPY etl.relationship_lille
from '/home/ant/Documents/01_projets/OMOP_Anesthesie/anesthesie_omop/etl/01_local_vocabularies/lille_vocabulary/4_relationship_lille.csv' 
DELIMITER ';' CSV HEADER;

select * from etl.relationship_lille;

-- Copie de la table relationship_snds dans la table relationship
------------------------------------------------------------------------------------------------------------------

SELECT * FROM omop.relationship where relationship_id in (select relationship_id from etl.relationship_lille);

DELETE FROM omop.relationship where relationship_id in (select relationship_id from etl.relationship_lille);

INSERT INTO omop.relationship
SELECT * FROM etl.relationship_lille;


drop sequence seq_concept_snds;
create sequence seq_concept_snds start 2000001000;

------------------------------------------------------------------------------------------------------------------
-- 5) concept_lille
------------------------------------------------------------------------------------------------------------------

--
--

DROP TABLE if exists etl.concept_lille;

CREATE TABLE if not exists etl.concept_lille
 (
  concept_id			INTEGER  		NOT NULL,
  concept_name		        VARCHAR 	        NOT NULL,
  domain_id			VARCHAR(20)		NOT NULL,
  vocabulary_id			VARCHAR(20)		NOT NULL,
  concept_class_id		VARCHAR(20)		NOT NULL,
  standard_concept		VARCHAR(1)		NULL,
  concept_code			VARCHAR(50)		NOT NULL,
  valid_start_date		DATE			NOT NULL,
  valid_end_date		DATE			NOT NULL,
  invalid_reason		VARCHAR(1)		NULL
);

delete from etl.concept_lille;

select * from etl.concept_lille;

--
-- 

DROP SEQUENCE if exists etl.concept_lille_id_seq;

CREATE SEQUENCE if not exists etl.concept_lille_id_seq INCREMENT 1 START 2000002000;

ALTER SEQUENCE etl.concept_lille_id_seq OWNER TO postgres;

------------------------------------------------------------------------------------------------------------------
-- 5.1) aims_unit_lille.csv
------------------------------------------------------------------------------------------------------------------

DROP TABLE if exists etl.concept_aims_unit_lille;

CREATE TABLE if not exists etl.concept_aims_unit_lille
 (
  concept_id			INTEGER  		NULL,
  concept_name		    VARCHAR			NOT NULL,
  domain_id				VARCHAR(20)		NOT NULL,
  vocabulary_id			VARCHAR(20)		NOT NULL,
  concept_class_id		VARCHAR(20)		NOT NULL,
  standard_concept		VARCHAR(1)		NULL,
  concept_code			VARCHAR(50)		NOT NULL,
  valid_start_date		DATE			NOT NULL,
  valid_end_date		DATE			NOT NULL,
  invalid_reason		VARCHAR(1)		NULL
);

--

delete from etl.concept_aims_unit_lille;

COPY etl.concept_aims_unit_lille
from '/home/ant/Documents/01_projets/OMOP_Anesthesie/anesthesie_omop/etl/01_local_vocabularies/lille_vocabulary/5_1_aims_unit_lille.csv' 
DELIMITER ';' CSV HEADER;

select * from etl.concept_aims_unit_lille;

--

insert into etl.concept_lille
select nextval('etl.concept_lille_id_seq'), s1.concept_name, s1.domain_id, 
s1.vocabulary_id, s1.concept_class_id, s1.standard_concept, 
s1.concept_code, s1.valid_start_date, s1.valid_end_date, s1.invalid_reason
from etl.concept_aims_unit_lille s1;

select * from etl.concept_lille order by concept_id;


------------------------------------------------------------------------------------------------------------------
-- 5.2) aims_measurement_lille.csv
------------------------------------------------------------------------------------------------------------------

DROP TABLE if exists etl.aims_measurement_lille;

CREATE TABLE if not exists etl.aims_measurement_lille
 (
  concept_id			INTEGER  		NULL,
  concept_name		    VARCHAR			NOT NULL,
  domain_id				VARCHAR(20)		NOT NULL,
  vocabulary_id			VARCHAR(20)		NOT NULL,
  concept_class_id		VARCHAR(20)		NOT NULL,
  standard_concept		VARCHAR(1)		NULL,
  concept_code			VARCHAR(50)		NOT NULL,
  valid_start_date		DATE			NOT NULL,
  valid_end_date		DATE			NOT NULL,
  invalid_reason		VARCHAR(1)		NULL
);

--

delete from etl.aims_measurement_lille;

COPY etl.aims_measurement_lille
from '/home/ant/Documents/01_projets/OMOP_Anesthesie/anesthesie_omop/etl/01_local_vocabularies/lille_vocabulary/5_2_aims_measurement_lille.csv' 
DELIMITER ';' CSV HEADER;

select * from etl.aims_measurement_lille;

--

insert into etl.concept_lille
select nextval('etl.concept_lille_id_seq'), s1.concept_name, s1.domain_id, 
s1.vocabulary_id, s1.concept_class_id, s1.standard_concept, 
s1.concept_code, s1.valid_start_date, s1.valid_end_date, s1.invalid_reason
from etl.aims_measurement_lille s1;

select * from etl.concept_lille order by concept_id desc;

------------------------------------------------------------------------------------------------------------------
-- 5.3) aims_episode_lille.csv
------------------------------------------------------------------------------------------------------------------

DROP TABLE if exists etl.aims_episode_lille;

CREATE TABLE if not exists etl.aims_episode_lille
 (
  concept_id			INTEGER  		NULL,
  concept_name		    VARCHAR			NOT NULL,
  domain_id				VARCHAR(20)		NOT NULL,
  vocabulary_id			VARCHAR(20)		NOT NULL,
  concept_class_id		VARCHAR(20)		NOT NULL,
  standard_concept		VARCHAR(1)		NULL,
  concept_code			VARCHAR(50)		NOT NULL,
  valid_start_date		DATE			NOT NULL,
  valid_end_date		DATE			NOT NULL,
  invalid_reason		VARCHAR(1)		NULL
);

--

delete from etl.aims_episode_lille;

COPY etl.aims_episode_lille
from '/home/ant/Documents/01_projets/OMOP_Anesthesie/anesthesie_omop/etl/01_local_vocabularies/lille_vocabulary/5_3_aims_episode_lille.csv' 
DELIMITER ';' CSV HEADER;

--

insert into etl.concept_lille
select nextval('etl.concept_lille_id_seq'), s1.concept_name, s1.domain_id, 
s1.vocabulary_id, s1.concept_class_id, s1.standard_concept, 
s1.concept_code, s1.valid_start_date, s1.valid_end_date, s1.invalid_reason
from etl.aims_episode_lille s1;

select * from etl.concept_lille order by concept_id desc;



------------------------------------------------------------------------------------------------------------------
-- 5.4) indicator_lille.csv
------------------------------------------------------------------------------------------------------------------

DROP TABLE if exists etl.indicator_lille;

CREATE TABLE if not exists etl.indicator_lille
 (
  concept_id			INTEGER  		NULL,
  concept_name		    VARCHAR			NOT NULL,
  domain_id				VARCHAR(20)		NOT NULL,
  vocabulary_id			VARCHAR(20)		NOT NULL,
  concept_class_id		VARCHAR(20)		NOT NULL,
  standard_concept		VARCHAR(1)		NULL,
  concept_code			VARCHAR(50)		NOT NULL,
  valid_start_date		DATE			NOT NULL,
  valid_end_date		DATE			NOT NULL,
  invalid_reason		VARCHAR(1)		NULL
);

--

delete from etl.indicator_lille;

COPY etl.indicator_lille
from '/home/ant/Documents/01_projets/omop_anesthesia/etl/01_local_vocabularies/lille_vocabulary/5_4_indicator_lille.csv' 
DELIMITER ';' CSV HEADER;

--

insert into etl.concept_lille
select nextval('etl.concept_lille_id_seq'), s1.concept_name, s1.domain_id, 
s1.vocabulary_id, s1.concept_class_id, s1.standard_concept, 
s1.concept_code, s1.valid_start_date, s1.valid_end_date, s1.invalid_reason
from etl.indicator_lille s1;

select * from etl.concept_lille order by concept_id desc;

------------------------------------------------------------------------------------------------------------------
-- 5.5) lille_threshold.csv
------------------------------------------------------------------------------------------------------------------

DROP TABLE if exists etl.threshold_lille;

CREATE TABLE if not exists etl.threshold_lille
 (
  concept_id			INTEGER  		NULL,
  concept_name		    VARCHAR			NOT NULL,
  domain_id				VARCHAR(20)		NOT NULL,
  vocabulary_id			VARCHAR(20)		NOT NULL,
  concept_class_id		VARCHAR(20)		NOT NULL,
  standard_concept		VARCHAR(1)		NULL,
  concept_code			VARCHAR(50)		NOT NULL,
  valid_start_date		DATE			NOT NULL,
  valid_end_date		DATE			NOT NULL,
  invalid_reason		VARCHAR(1)		NULL
);

--

delete from etl.threshold_lille;

COPY etl.threshold_lille
from '/home/ant/Documents/01_projets/omop_anesthesia/etl/01_local_vocabularies/lille_vocabulary/5_5_threshold_lille.csv' 
DELIMITER ';' CSV HEADER;

--

insert into etl.concept_lille
select nextval('etl.concept_lille_id_seq'), s1.concept_name, s1.domain_id, 
s1.vocabulary_id, s1.concept_class_id, s1.standard_concept, 
s1.concept_code, s1.valid_start_date, s1.valid_end_date, s1.invalid_reason
from etl.aims_episode_lille s1;

select * from etl.concept_lille order by concept_id desc desc;

------------------------------------------------------------------------------------------------------------------
-- 5.9 new_concept_lille.csv
------------------------------------------------------------------------------------------------------------------


DROP TABLE if exists etl.new_concept_lille;

CREATE TABLE if not exists etl.new_concept_lille
 (
  concept_id			INTEGER  		NULL,
  concept_name		    VARCHAR			NOT NULL,
  domain_id				VARCHAR(20)		NOT NULL,
  vocabulary_id			VARCHAR(20)		NOT NULL,
  concept_class_id		VARCHAR(20)		NOT NULL,
  standard_concept		VARCHAR(1)		NULL,
  concept_code			VARCHAR(50)		NOT NULL,
  valid_start_date		DATE			NOT NULL,
  valid_end_date		DATE			NOT NULL,
  invalid_reason		VARCHAR(1)		NULL
);

delete from etl.new_concept_lille;

--

COPY etl.new_concept_lille
from '/home/ant/Documents/01_projets/OMOP_Anesthesie/anesthesie_omop/etl/01_local_vocabularies/lille_vocabulary/5_99_concept_lille.csv' 
DELIMITER ';' CSV HEADER;

insert into etl.concept_lille
select nextval('etl.concept_lille_id_seq'), s1.concept_name, s1.domain_id, 
s1.vocabulary_id, s1.concept_class_id, s1.standard_concept, 
s1.concept_code, s1.valid_start_date, s1.valid_end_date, s1.invalid_reason
from etl.new_concept_lille s1;

select * from etl.concept_lille order by concept_id desc;

------------------------------------------------------------------------------------------------------------------
-- Insertion of local concepts into concept
------------------------------------------------------------------------------------------------------------------

DELETE FROM omop.concept where concept_id >= 2000002000;

DELETE FROM omop.concept where concept_id in (select concept_id from etl.concept_lille);

INSERT INTO omop.concept
SELECT * FROM etl.concept_lille;

------------------------------------------------------------------------------------------------------------------
-- 6) 6_aims_concept_relationship.csv
------------------------------------------------------------------------------------------------------------------

DROP TABLE if exists etl.prop_concept_relationship_lille;

CREATE TABLE if not exists etl.prop_concept_relationship_lille
 (
	concept_name_1				VARCHAR			NULL
	 , vocabulary_id_1			VARCHAR(20)		NOT NULL
	 , concept_code_1				VARCHAR(50)		NOT NULL
	 , concept_id_2				integer			NULL
	 , concept_name_2			VARCHAR			NULL
	 , relationship_id			VARCHAR(20)		NULL
	 , valid_start_date			DATE			NULL
	 , valid_end_date			DATE			NULL
	 , invalid_reason			VARCHAR(1)		NULL
	 );

--

COPY etl.prop_concept_relationship_lille
from '/home/ant/Documents/01_projets/OMOP_Anesthesie/anesthesie_omop/etl/01_local_vocabularies/lille_vocabulary/6_aims_concept_relationship.csv' 
DELIMITER ';' CSV HEADER;

-- 

drop table if exists etl.temp_concept_relationship;

create table if not exists etl.temp_concept_relationship (
	concept_id_1 integer NOT NULL
	, concept_id_2 integer NOT NULL
	, relationship_id character varying(20)
	, valid_start_date date NOT NULL
	, valid_end_date date NOT NULL
	, invalid_reason character varying(1)
);

--

delete from etl.temp_concept_relationship;

insert into etl.temp_concept_relationship
select c1.concept_id as concept_id_1
	, c2.concept_id as concept_id_2
	, prop.relationship_id
	, prop.valid_start_date
	, prop.valid_end_date
	, prop.invalid_reason
from etl.prop_concept_relationship_lille prop
left outer join omop.concept c1
on prop.vocabulary_id_1 = c1.vocabulary_id
and prop.concept_code_1 = c1.concept_code
left outer join omop.concept c2
on prop.concept_id_2 = c2.concept_id
where concept_id_2 is not null;

--

select * from omop.concept_relationship t1 
where exists (select * from etl.temp_concept_relationship t2 where
			 t1.concept_id_1 = t2.concept_id_1
			 and t1.concept_id_2 = t2.concept_id_2);
			 
delete from omop.concept_relationship t1 
where exists (select * from etl.temp_concept_relationship t2 where
			 t1.concept_id_1 = t2.concept_id_1
			 and t1.concept_id_2 = t2.concept_id_2);
			 
--

insert into omop.concept_relationship 
select * from etl.temp_concept_relationship;


---------------------

select vocabulary_id, count(*) 
from etl.concept_lille 
group by vocabulary_id 
order by count(*) desc;
