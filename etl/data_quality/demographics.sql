-----


-----

-- Demographics

-- Age
select	percentile_disc(0.25) WITHIN GROUP (ORDER BY o.value_as_number) as quart_1,
		percentile_disc(0.5) WITHIN GROUP (ORDER BY o.value_as_number) as quart_2,
		percentile_disc(0.75) WITHIN GROUP (ORDER BY o.value_as_number) as quart_3
from omop.observation o
where o.observation_concept_id = 4265453;

-- Sex
select concept_name, count(*)
from omop.person p
left outer join omop.concept c
on p.gender_concept_id = c.concept_id
group by c.concept_name;

-- Weight
select	percentile_disc(0.25) WITHIN GROUP (ORDER BY o.value_as_number) as quart_1,
		percentile_disc(0.5) WITHIN GROUP (ORDER BY o.value_as_number) as quart_2,
		percentile_disc(0.75) WITHIN GROUP (ORDER BY o.value_as_number) as quart_3
from omop.observation o
where o.observation_concept_id = 37111521;

-- BMI
select	percentile_disc(0.25) WITHIN GROUP (ORDER BY o.value_as_number) as quart_1,
		percentile_disc(0.5) WITHIN GROUP (ORDER BY o.value_as_number) as quart_2,
		percentile_disc(0.75) WITHIN GROUP (ORDER BY o.value_as_number) as quart_3
from omop.observation o
where o.observation_concept_id = 4245997;

-- ASA Status
select c.concept_name, count(*)
from omop.observation o, omop.concept c
where o.value_as_concept_id = c.concept_id
and o.observation_concept_id = 4185946
group by c.concept_name;

select * from omop.concept limit 100;