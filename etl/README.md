# Anesthesia data into OMOP CDM

This repository contains scripts and documentation about the **semantic and structural mapping** of anesthesia data into OMOP CDM.

**00_ddl** constains SQL scripts for creating the OMOP CDM.<br />
**01_local_vocabularies** constains vocabulary files and SQL scripts to load anesthesia vocabulary into OMOP CDM (Standardized Vocabularies).<br />
**02_etl_scripts** constains the SQL scripts to load raw data into OMOP CDM (Standardized Clinical Data Tables).<br />
**data_quality** constains SQL scripts for quality assessement of ETL process.<br />
**raw_data_samples** constains fictitious data samples to test ETL scripts.<br />