---

-- Temporary table for new concepts

DROP TABLE if exists etl.concept_new;

CREATE TABLE etl.concept_new
(
    concept_id 				integer NOT NULL,
    concept_name 			character varying(255) NOT NULL,
    domain_id 				character varying(20) NOT NULL,
    vocabulary_id 			character varying(20) NOT NULL,
    concept_class_id		character varying(20) NOT NULL,
    standard_concept 		character varying(1) ,
    concept_code 			character varying(50) NOT NULL,
    valid_start_date 		date NOT NULL,
    valid_end_date 			date NOT NULL,
    invalid_reason 			character varying(1)
) TABLESPACE tbs_crypt;


COPY etl.concept_new FROM '/home/ant/Documents/01_projets/OMOP_Anesthesie/anesthesie_omop/etl/01_new_concepts/new_concepts.csv' 
WITH DELIMITER E';' CSV HEADER QUOTE E'"' ENCODING 'UTF-8'
;

insert into omop.concept
select *
from etl.concept_new;

delete from omop.concept where concept_id in (select concept_id from etl.concept_new);

