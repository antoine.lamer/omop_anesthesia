----------------------------------------
--
-- ETL OMOP ANESTHESIE
--
-- Antoine Lamer
--
-- 03/12/19
--
----------------------------------------

/*
Schemas:
raw_data = schema with raw data
etl = schema for etl transformations
omop = final OMOP schema
*/

-- Person
----------------------------------------------------

-- Extract patient_id, birth_date, sex
----------------------------------------------------

drop table etl.person_extract;

create table etl.person_extract
as
-- Raw data
select id_patient as person_source_value
		, date_naissance as birth_date
		, sexe_nm as gender_source_value
from raw_data.intervention_patient -- 1 row per operation
group by id_patient, date_naissance, sexe_nm;


-- Mapping with concept_id
----------------------------------------------------

drop table if exists etl.person_tr;

create table if not exists etl.person_tr
as
select 0::integer as person_id
		, CASE
     		WHEN gender_source_value = 0  THEN  8507 -- male
     		WHEN gender_source_value = 1  THEN  8532 -- female
     		ELSE 8551 -- unknown
  		END AS gender_concept_id
		, extract(year from birth_date) as year_of_birth
		, extract(month from birth_date) as month_of_birth
		, extract(day from birth_date) as day_of_birth
		, birth_date as birth_datetime
		, null::date as death_datetime -- todo
		, 0::integer as race_concept_id
		, 0::integer as ethnicity_concept_id
		, 0::integer as location_id -- todo
		, 0::integer as provider_id
		, 0::integer as care_site_id -- todo
		, person_source_value
		, gender_source_value
		, 0::integer as gender_source_concept_id -- todo
		, 0::integer as race_source_value
		, 0::integer as race_source_concept_id
		, 0::integer as ethnicity_source_value
		, 0::integer as ethnicity_source_concept_id
from etl.person_extract;

-- Load into omop_anesthesia.person
----------------------------------------------------

truncate omop.person;
alter sequence etl.person_id_seq restart with 1;

insert into omop.person (
	person_id
	, gender_concept_id
	, year_of_birth
	, month_of_birth
	, day_of_birth
	, birth_datetime
	, death_datetime
	, race_concept_id
	, ethnicity_concept_id
	, location_id
	, provider_id
	, care_site_id
	, person_source_value
	, gender_source_value
	, gender_source_concept_id
	, race_source_value
	, race_source_concept_id
	, ethnicity_source_value
	, ethnicity_source_concept_id
)
-----
select nextval('etl.person_id_seq')
	, gender_concept_id
	, year_of_birth
	, month_of_birth
	, day_of_birth
	, birth_datetime
	, death_datetime
	, race_concept_id
	, ethnicity_concept_id
	, location_id
	, provider_id
	, care_site_id
	, person_source_value
	, gender_source_value
	, gender_source_concept_id
	, race_source_value
	, race_source_concept_id
	, ethnicity_source_value
	, ethnicity_source_concept_id
from etl.person_tr;

select * from omop.person;
