----------------------------------------
--
-- ETL OMOP ANESTHESIE
--
-- Table : CONDITION_OCCURRENCE
--
-- Antoine Lamer
--
-- 14/12/19
--
-- 1. Extract patient_id
-- date of anesthesia consultation
-- date of operating room
-- dates of post-anesthesia care unit
-- 2. Transform and mapp to OHDSI concept_id
-- 3. Load into omop_anesthesia.visit_detail
--
----------------------------------------

/*
Schemas:
raw_data = schema with raw data
etl = schema for etl transformations
omop = final OMOP schema
*/

-- Person
------------------------------------------------------------


-- 1. Extract visit_start_date, visit_end_date, visit_source_value,
-- admitted_from_source_value, discharge_to_source_value
-- from the source table
------------------------------------------------------------
drop table if exists etl.condition_occurrence_extract;

create table if not exists etl.condition_occurrence_extract
as
-- Raw data -- Operating room
select id_intervention as visit_detail_source_value
		, id_mouvement as visit_detail_source_value_2 -- temporary field
		, code_diagnostic as condition_source_value
		, type_diagnostic as condition_type_source_value
from raw_data.diagnostic;


select * from raw_data.diagnostic limit 100;

-- 2. Transform
--
-- person_id
-- condition_concept_id
-- condition_start_date
-- condition_end_date
-- visit_occurrence_id
-- visit_detail_id
--
--
-- #### TODO rajouter l'id_intervention
--
----------------------------------------------------

drop table if exists etl.visit_detail_tr;

create table if not exists etl.visit_detail_tr
as
select 0 as	condition_occurrence_id
	, coalesce(person_id, 0) as person_id
	, 0 as condition_concept_id  -- TODO
	, coalesce(vd.visit_detail_start_date, null::date) as condition_start_date
	, condition_start_datetime
	, coalesce(vd.visit_detail_end_date, null::date) as condition_end_date
	, condition_end_datetime
	, case
		when coe.condition_type_source_value = 'P' then 44786627 -- Primary condition
		when coe.condition_type_source_value = 'S' then 44786629 -- Secondary condition
		when coe.condition_type_source_value = 'R' then 44786629
		else 0
	, condition_status_concept_id
	, stop_reason
	, 0 as provider_id
	, coalesce(visit_occurrence_id, 0) as visit_occurrence_id
	, coalesce(visit_detail_id, 0) as visit_detail_id
	, condition_source_value
	, 0 as condition_source_concept_id -- TODO
	, null::character as condition_status_source_value
from etl.condition_occurrence_extract coe
left outer join omop.visit_detail vd
on vd.visit_detail_source_value = coe.visit_detail_source_value
;

-- 3. Load into omop_anesthesia.visit_occurrence
----------------------------------------------------

truncate omop.visit_detail;
alter sequence etl.visit_detail_id_seq restart with 1;

insert into  omop.condition_occurrence(
	condition_occurrence_id
	, person_id
	, condition_concept_id
	, condition_start_date
	, condition_start_datetime
	, condition_end_date
	, condition_end_datetime
	, condition_type_concept_id
	, condition_status_concept_id
	, stop_reason
	, provider_id
	, visit_occurrence_id
	, visit_detail_id
	, condition_source_value
	, condition_source_concept_id
	, condition_status_source_value
)
-----
select nextval('etl.condition_occurrence_id_seq')
	, person_id
	, condition_concept_id
	, condition_start_date
	, condition_start_datetime
	, condition_end_date
	, condition_end_datetime
	, condition_type_concept_id
	, condition_status_concept_id
	, stop_reason
	, provider_id
	, visit_occurrence_id
	, visit_detail_id
	, condition_source_value
	, condition_source_concept_id
	, condition_status_source_value
from etl.condition_occurrence_tr;