----------------------------------------
--
-- ETL OMOP ANESTHESIE
--
-- Antoine Lamer
--
-- 03/12/19
--
-- 1. Extract patient_id, birth_date, sex from the source table
-- 2. Transform and mapp to OHDSI concept_id
-- 3. Load into omop_anesthesia.visit_occurrence
--
----------------------------------------

/*
Schemas:
raw_data = schema with raw data
etl = schema for etl transformations
omop = final OMOP schema
*/


-- 1. Extract measurement value, parameter, unit, date and 
-- operation identifier from the source table
------------------------------------------------------------

drop table if exists etl.measurement_extract;

create table if not exists etl.measurement_extract as 
--
select id_intervention::text as visit_detail_source_value
		, id_parametre::text as measurement_source_value
		, valeur as value_source_value
		, date_sauvegarde as measurement_datetime
		, id_unite::text as unit_source_value
from raw_data.mesure;

-- 2. Transform
--
-- Mapping with concept_id
-- 
-- person_id
-- measurement_concept_id
-- measurement_type_concept_id
-- unit_concept_id
-- ..
--
-- Value, keep only numeric value ?
-- 
----------------------------------------------------

-- 

drop table if exists etl.measurement_tr;

create table if not exists etl.measurement_tr as 
select 	0 as measurement_id
		, coalesce(vd.person_id, 0) as person_id
		, coalesce(cm.concept_id, 0) as measurement_concept_id
		, null::date as measurement_date
		, measurement_datetime
		, null::varchar as measurement_time
		, 0::integer as measurement_type_concept_id  -- TODO
		, null::integer as operator_concept_id
		, value_source_value::numeric as value_as_number -- keep only numeric value
		, null::integer as value_as_concept_id
		, coalesce(cu.concept_id, 0) as unit_concept_id
		, null::numeric as range_low
		, null::numeric as range_high
		, 0 as provider_id
		, coalesce(vd.visit_occurrence_id, 0) as visit_occurrence_id
		, coalesce(vd.visit_detail_id, 0) as visit_detail_id
		, measurement_source_value
		, 0 as measurement_source_concept_id -- TODO
		, unit_source_value
		, value_source_value::text
from etl.measurement_extract me
left outer join omop.visit_detail vd
on me.visit_detail_source_value = vd.visit_detail_source_value
and vd.visit_detail_concept_id = 2100000000
left outer join omop.concept cm
on me.measurement_source_value = cm.concept_code
and cm.vocabulary_id = 'Lille AIMS Meas'
left outer join omop.concept cu
on me.unit_source_value = cm.concept_code
and cu.vocabulary_id = 'Lille AIMS Unit'
;

-- Indicateurs
-------------------------------------------------------------------------------

drop table if exists etl.measurement_indicator_extract;

create table if not exists etl.measurement_indicator_extract as
select id_intervention::text as visit_detail_source_value
		, id_indicateur_mesure::text as measurement_source_value
		, date_debut_fenetre as measurement_datetime
		, valeur as value_as_number
from raw_data.interv_indicateur;

--

drop table if exists etl.measurement_indicator_tr;

create table if not exists etl.measurement_indicator_tr as
select 0::integer as measurement_id
		, vd.person_id
		, 0::integer as measurement_concept_id
		, null::date as measurement_date
		, mie.measurement_datetime
		, null::varchar as measurement_time
		, 0::integer as measurement_type_concept_id -- todo
		, null::integer as operator_concept_id
		, mie.value_as_number
		, null::integer as value_as_concept_id
		, null::integer as unit_concept_id
		, null::numeric as range_low
		, null::numeric as range_high
		, 0 as provider_id
		, coalesce(vd.visit_occurrence_id, 0) as visit_occurrence_id
		, coalesce(vd.visit_detail_id, 0) as visit_detail_id
		, 0::text as measurement_source_value
		, c.concept_id as measurement_source_concept_id -- TODO
		, null::varchar as unit_source_value
		, null::varchar as value_source_value
from etl.measurement_indicator_extract mie
left outer join omop.visit_detail vd
on mie.visit_detail_source_value = vd.visit_detail_source_value
and vd.visit_detail_concept_id = 2100000000
left outer join omop.concept c
on mie.measurement_source_value = c.concept_code
and c.vocabulary_id = 'Lille Indicator'
;

select * from etl.measurement_indicator_tr where person_id is nnull limit 100;

select tr.person_id, tr.measurement_datetime, tr.value_as_number,
c.concept_name  from etl.measurement_indicator_tr tr
inner join omop.concept c
on tr.measurement_source_concept_id = c.concept_id;

select count(*) from etl.measurement_indicator_tr

-- 3. Load into omop_anesthesia.person
----------------------------------------------------


truncate omop.measurement;
alter sequence etl.measurement_id_seq restart with 1;

insert into omop.measurement (
		measurement_id
		, person_id
		, measurement_concept_id
		, measurement_date
		, measurement_datetime
		, measurement_time
		, measurement_type_concept_id
		, operator_concept_id
		, value_as_number
		, value_as_concept_id
		, unit_concept_id
		, range_low
		, range_high
		, provider_id
		, visit_occurrence_id
		, visit_detail_id
		, measurement_source_value
		, measurement_source_concept_id
		, unit_source_value
		, value_source_value
)
-----
select nextval('etl.measurement_id_seq')
		, person_id
		, measurement_concept_id
		, measurement_date
		, measurement_datetime
		, measurement_time
		, measurement_type_concept_id
		, operator_concept_id
		, value_as_number
		, value_as_concept_id
		, unit_concept_id
		, range_low
		, range_high
		, provider_id
		, visit_occurrence_id
		, visit_detail_id
		, measurement_source_value
		, measurement_source_concept_id
		, unit_source_value
		, value_source_value
from etl.measurement_tr
--
--
--
union 
select nextval('etl.measurement_id_seq')
		, person_id
		, measurement_concept_id
		, measurement_date
		, measurement_datetime
		, measurement_time
		, measurement_type_concept_id
		, operator_concept_id
		, value_as_number
		, value_as_concept_id
		, unit_concept_id
		, range_low
		, range_high
		, provider_id
		, visit_occurrence_id
		, visit_detail_id
		, measurement_source_value
		, measurement_source_concept_id
		, unit_source_value
		, value_source_value
from etl.measurement_indicator_tr
where person_id is not null
;

select * from omop.measurement where measurement_concept_id = 2000003340;

select * from omop.concept where concept_id = 2000003340;





