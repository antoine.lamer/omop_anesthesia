----------------------------------------
--
-- ETL OMOP ANESTHESIE
--
-- Table : PROCEDURE_OCCURRENCE
--
-- Antoine Lamer
--
-- 14/12/19
--
-- 1. Extract patient_id
-- date of act
-- date of operating room
-- dates of post-anesthesia care unit
-- 2. Transform and mapp to OHDSI concept_id
-- 3. Load into omop.procedure_occurrence
--
----------------------------------------

-- 1. Extract procedure_concept_source_value, procedure_date
-- from
-- source 1 : administrative database
-- source 2 : AIMS
------------------------------------------------------------
drop table if exists etl.procedure_occurrence_extract;

create table if not exists etl.procedure_occurrence_extract
as
-- Raw data -- procedure from the administrative database
select id_intervention as visit_detail_source_value
		, id_mouvement as visit_detail_source_value_2 -- temporary field
		, code_acte_ccam as procedure_source_value
		, activite
		, phase
		, nombre_actes as quantity
from raw_data.acte;

select * from raw_data.acte limit 100;

-- 2. Transform
--
-- person_id
-- procedure_concept_id
-- visit_occurrence_id
-- visit_detail_id
--
-- #### TODO rajouter l'id_intervention
--
----------------------------------------------------

drop table if exists etl.procedure_occurrence_tr;

create table if not exists etl.procedure_occurrence_tr
as
select 0 as	procedure_occurrence_id
	, coalesce(person_id, 0) as person_id
	, 0 as procedure_concept_id  -- TODO
	, procedure_date
	, procedure_datetime
	, procedure_type_concept_id
	, modifier_concept_id
	, quantity
	, 0 as provider_id
	, coalesce(visit_occurrence_id, 0) as visit_occurrence_id
	, coalesce(visit_detail_id, 0) as visit_detail_id
	, procedure_source_value
	, 0 as procedure_source_concept_id -- TODO
	, null::character as modifier_source_value
from etl.condition_occurrence_extract coe
left outer join omop.visit_detail vd
on vd.visit_detail_source_value = coe.visit_detail_source_value
;

-- 3. Load into omop_anesthesia.visit_occurrence
----------------------------------------------------

truncate omop.procedure_occurrence;
alter sequence etl.procedure_occurrence_id_seq restart with 1;

insert into  omop.procedure_occurrence(
	procedure_occurrence_id
	, person_id
	, procedure_concept_id
	, procedure_date
	, procedure_datetime
	, procedure_type_concept_id
	, modifier_concept_id
	, quantity
	, provider_id
	, visit_occurrence_id
	, visit_detail_id
	, procedure_source_value
	, procedure_source_concept_id
	, modifier_source_value
)
-----
select nextval('etl.procedure_occurrence_id_seq')
	, person_id
	, procedure_concept_id
	, procedure_date
	, procedure_datetime
	, procedure_type_concept_id
	, modifier_concept_id
	, quantity
	, provider_id
	, visit_occurrence_id
	, visit_detail_id
	, procedure_source_value
	, procedure_source_concept_id
	, modifier_source_value
from etl.procedure_occurrence_tr;