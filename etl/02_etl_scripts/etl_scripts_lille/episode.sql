----------------------------------------
--
-- ETL OMOP ANESTHESIE
--
-- Antoine Lamer
--
-- 03/12/19
--
-- 1. Extract start and end date of episode
-- 2. Transform and mapp to OHDSI concept_id
-- 3. Load into omop.episode
--
----------------------------------------


-- 1. Extract 
------------------------------------------------------------

-- V1

drop table if exists etl.episode_event_extract;

create table if not exists etl.episode_event_extract as 
--
-- Operating room Episode
--
select 
		0::integer as episode_id
		, 4021813 as episode_concept_id
		, coalesce(p.person_id, 0) as person_id
		, date_debut_bloc as episode_start_datetime
		, date_fin_bloc as episode_end_datetime
		, 32544 as episode_type_concept_id -- Episode defined in EHR
		, 1::integer as episode_number
		, coalesce(vd.visit_occurrence_id, 0) as visit_occurrence_id
		, coalesce(vd.visit_detail_id, 0) as visit_detail_id
		, id_patient::text as person_source_value
		, id_intervention::text as visit_detail_source_value
from raw_data.intervention_patient ip
left outer join omop.person p
on ip.id_patient::text = p.person_source_value
left outer join omop.visit_detail vd
on ip.id_intervention::text = vd.visit_detail_source_value
where date_debut_bloc is not null and  date_fin_bloc is not null
--
-- SSPI Episode
--
union
select 
		0::integer as episode_id
		, 4134563 as episode_concept_id
		, coalesce(p.person_id, 0) as person_id
		, date_debut_sspi as episode_start_datetime
		, date_fin_sspi as episode_end_datetime
		, 32544 as episode_type_concept_id -- Episode defined in EHR
		, 1::integer as episode_number
		, coalesce(vd.visit_occurrence_id, 0) as visit_occurrence_id
		, coalesce(vd.visit_detail_id, 0) as visit_detail_id
		, id_patient::text as person_source_value
		, id_intervention::text as visit_detail_source_value
from raw_data.intervention_patient ip
left outer join omop.person p
on ip.id_patient::text = p.person_source_value
left outer join omop.visit_detail vd
on ip.id_intervention::text = vd.visit_detail_source_value
where date_debut_sspi is not null and  date_fin_sspi is not null
;

--

select 	0::integer as episode_id
		, 4134563 as episode_concept_id
		, coalesce(p.person_id, 0) as person_id
		, date_debut_sspi as episode_start_datetime
		, date_fin_sspi as episode_end_datetime
		, 32544 as episode_type_concept_id -- Episode defined in EHR
		, 1::integer as episode_number
		, coalesce(vd.visit_occurrence_id, 0) as visit_occurrence_id
		, coalesce(vd.visit_detail_id, 0) as visit_detail_id
		, id_patient::text as person_source_value
		, id_intervention::text as visit_detail_source_value
from raw_data.interv_fenetre_etude
left outer join omop.visit_detail vd
on ip.id_intervention::text = vd.visit_detail_source_value

-- V2

drop table etl.episode_extract;

create table etl.episode_extract as
select 0::integer as episode_id
		, 0::integer as episode_concept_id
		, coalesce(vd.person_id, 0) as person_id
		, date_debut_fenetre as episode_start_datetime
		, date_fin_fenetre as episode_end_datetime
		, 0::integer as episode_parent_id
		, 32544 as episode_type_concept_id -- Episode defined in EHR
		, 0::integer episode_object_concept_id -- TODO
		, num_fenetre as episode_number
		, coalesce(vd.visit_occurrence_id, 0) as visit_occurrence_id
		, coalesce(vd.visit_detail_id, 0) as visit_detail_id
		, ife.id_intervention::text as visit_detail_source_value
		, id_fenetre_etude::text episode_source_value     -- TODO 
		, 0::integer episode_source_concept_id -- TODO
from raw_data.interv_fenetre_etude ife
left outer join omop.visit_detail vd
on ife.id_intervention::text = vd.visit_detail_source_value;

--

create table etl.episode_tr as
select ee.episode_id
		, coalesce(cr.concept_id_2, 0) as episode_concept_id
		, ee.person_id
		, ee.episode_start_datetime
		, ee.episode_end_datetime
		, ee.episode_parent_id
		, ee.episode_type_concept_id
		, ee.episode_object_concept_id
		, ee.episode_number
		, ee.visit_occurrence_id
		, ee.visit_detail_id
		, ee.visit_detail_source_value
		, ee.episode_source_value
		, coalesce(c.concept_id, 0) as episode_source_concept_id
from etl.episode_extract ee
left outer join omop.concept c
on ee.episode_source_value = c.concept_code
and c.vocabulary_id = 'Lille Episode'
left outer join omop.concept_relationship cr
on c.concept_id = cr.concept_id_1
and relationship_id = 'L Ep To Episode';

-- UPD Ad hoc pour les épisodes créés à Lille
----------------------------------------------

-- Induction - Incision

select * from etl.episode_tr where episode_source_value = '71';

update etl.episode_tr set episode_concept_id = 2000002912
where episode_source_value = '71';

update etl.episode_tr set episode_concept_id = 2000002913
where episode_source_value = '116';

select * from etl.concept_lille order by concept_id desc;

------------------------------------------------------------------

truncate omop.episode;
alter sequence etl.episode_id_seq restart with 1;

insert into omop.episode (
	episode_id
	, person_id
	, episode_concept_id       
	, episode_start_datetime   
	, episode_end_datetime     
	, episode_parent_id        
	, episode_number           
	, episode_object_concept_id
	, episode_type_concept_id  
	, visit_occurrence_id
	, visit_detail_id
	, episode_source_value     
	, episode_source_concept_id
	)
select nextval('etl.episode_id_seq')
	, person_id
	, episode_concept_id       
	, episode_start_datetime   
	, episode_end_datetime     
	, episode_parent_id        
	, episode_number           
	, episode_object_concept_id -- TODO
	, episode_type_concept_id  
	, visit_occurrence_id
	, visit_detail_id
	, episode_source_value     -- TODO 
	, episode_source_concept_id -- TODO
from etl.episode_tr;

select * from omop.episode;



insert into omop.episode_event (
  , episode_id
  , event_id 		                    
  , episode_event_field_concept_id
)
;