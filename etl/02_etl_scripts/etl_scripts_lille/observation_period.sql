----------------------------------------
--
-- ETL OMOP ANESTHESIE
--
-- Antoine Lamer
--
-- 03/12/19
--
----------------------------------------

/*
Schemas:
raw_data = schema with raw data
etl = schema for etl transformations
omop = final OMOP schema
*/

-- Observation_period
--
-- For an operation or a post-anesthesia care unit stay,
-- if there is the corresponding visit_occurence stay
-- it provides start and end dates of observation period
-- if not, keep dates of operation or PACU
----------------------------------------------------

-- Extract patient_id, birth_date, sex
----------------------------------------------------

drop table etl.observation_period_extract;

create table etl.person_extract
as
-- Raw data
select id_patient as person_source_value
		, date_naissance as birth_date
		, sexe_nm as gender_source_value
from raw_data.intervention_patient -- 1 row per operation
group by id_patient, date_naissance, sexe_nm;


select
from omop.visit_detail vd
-- where filtrer sur  

drop table if exists etl.observation_period_extract;

create table if not exists etl.observation_period_extract as
select person_id
		, visit_start_date as observation_period_start_date
		, visit_end_date as observation_period_end_date
from omop.visit_occurrence vo;

-- Mapping with concept_id
----------------------------------------------------

drop table if exists etl.observation_period_mapping;

create table if not exists etl.observation_period_mapping
as
select 0::integer as observation_period_id
		, person_id
		, observation_period_start_date
		, observation_period_end_date
		, 44814724 as period_type_concept_id -- Period covering healthcare encounters
from etl.observation_period_extract;

-- Load into omop_anesthesia.person
----------------------------------------------------

truncate omop.observation_period;
alter sequence etl.observation_period_id_seq restart with 1;

insert into omop.observation_period (
	observation_period_id
	, person_id
	, observation_period_start_date
	, observation_period_end_date
	, period_type_concept_id
)
-----
select nextval('etl.observation_period_id_seq')
	observation_period_id
	, person_id
	, observation_period_start_date
	, observation_period_end_date
	, period_type_concept_id
from etl.observation_period_mapping;

select * from omop.observation_period;
